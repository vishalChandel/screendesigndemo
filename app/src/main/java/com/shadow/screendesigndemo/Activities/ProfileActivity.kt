package com.shadow.screendesigndemo.Activities

import android.content.Intent
import android.os.Bundle
import android.widget.GridView
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shadow.screendesigndemo.Adapter.bucketListAdapter
import com.shadow.screendesigndemo.Adapter.shotsListAdapter
import com.shadow.screendesigndemo.R

class ProfileActivity : AppCompatActivity() {
    var bucketListAdapter: bucketListAdapter? = null

    // creating arrayList
    var ImageList: ArrayList<Int>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        var menuIV1 = findViewById<ImageView>(R.id.menuIV1)!!
        menuIV1.setOnClickListener {
            val intent = Intent(this, PerformanceActivity::class.java)
            this.startActivity(intent)
        }
        var backIV = findViewById<ImageView>(R.id.backIV)!!
        backIV.setOnClickListener {
            val intent = Intent(this, DetailsActivity::class.java)
            this.startActivity(intent)
        }
        lateinit var gridView1: GridView


        var ShotsList = intArrayOf(
                R.drawable.shot,
                R.drawable.shot,
                R.drawable.shot,
                R.drawable.shot,
                R.drawable.shot,
                R.drawable.shot,
                R.drawable.shot,
                R.drawable.shot,
                R.drawable.shot,
                R.drawable.shot,
                R.drawable.shot

        )
        gridView1 = findViewById(R.id.gridView2)
        val myAdapter1 = shotsListAdapter(this@ProfileActivity, ShotsList)
        gridView1.adapter = myAdapter1

        var recyclerView: RecyclerView = findViewById(R.id.bucketListRecyclerview)

        // setting recyclerView layoutManager
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.setLayoutManager(layoutManager)
        bucketListAdapter = bucketListAdapter()

        // setting recycle view adapter
        recyclerView.setAdapter(bucketListAdapter)

    }
}
