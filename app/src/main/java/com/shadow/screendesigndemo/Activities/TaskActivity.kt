package com.shadow.screendesigndemo.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shadow.screendesigndemo.Adapter.SearchByListAdapter
import com.shadow.screendesigndemo.Adapter.TaskList1Adapter
import com.shadow.screendesigndemo.Adapter.TaskList2Adapter
import com.shadow.screendesigndemo.R

class TaskActivity : AppCompatActivity() {
    var taskList1Adapter: TaskList1Adapter? = null
    var taskList2Adapter: TaskList2Adapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)
        var taskIV = findViewById<ImageView>(R.id.taskIV)!!
        taskIV.setOnClickListener{
            val intent = Intent(this, AccountsActivity::class.java)
            this.startActivity(intent)
        }
        var recyclerView: RecyclerView = findViewById(R.id.taskRecyclerView1)
        var recyclerView2: RecyclerView = findViewById(R.id.taskRecyclerView2)

        // setting recyclerView layoutManager
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL, false
        )
        // setting recyclerView layoutManager
        val layoutManager2: RecyclerView.LayoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL, false
        )
        recyclerView.setLayoutManager(layoutManager)
        taskList1Adapter = TaskList1Adapter()

        // setting recycle view adapter
        recyclerView.setAdapter(taskList1Adapter)
        recyclerView2.setLayoutManager(layoutManager2)
        taskList2Adapter = TaskList2Adapter()

        // setting recycle view adapter
        recyclerView2.setAdapter(taskList2Adapter)
    }
    }
