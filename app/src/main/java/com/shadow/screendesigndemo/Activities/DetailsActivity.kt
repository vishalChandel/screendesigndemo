package com.shadow.screendesigndemo.Activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.shadow.screendesigndemo.Adapter.DetailsActivityAdapter
import com.shadow.screendesigndemo.R

class DetailsActivity : AppCompatActivity() {
    private var viewPager: ViewPager? = null
    private var chat: TextView? = null
    private var call: TextView? = null
    private var status: TextView? = null

    private var pagerViewAdapter: DetailsActivityAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        chat = findViewById(R.id.chat)
        status = findViewById(R.id.status)
        call = findViewById(R.id.call)
         var menuIV = findViewById<ImageView>(R.id.menuIV)!!
        menuIV.setOnClickListener{
            val intent = Intent(this, ProfileActivity::class.java)
            this.startActivity(intent)
        }

        var view1: View = findViewById(R.id.view1)
        var view2: View = findViewById(R.id.view2)
        var view3: View = findViewById(R.id.view3)
        viewPager = findViewById(R.id.FragmentContainer)
        chat?.setOnClickListener(View.OnClickListener {
            viewPager?.setCurrentItem(0)
            view1.setVisibility(View.VISIBLE)
            view2.setVisibility(View.GONE)
            view3.setVisibility(View.GONE)

        })
        status?.setOnClickListener(View.OnClickListener { viewPager?.setCurrentItem(1)

            view1.setVisibility(View.GONE)
            view2.setVisibility(View.VISIBLE)
            view3.setVisibility(View.GONE)

           })
        call?.setOnClickListener(View.OnClickListener { viewPager?.setCurrentItem(2)
            view1.setVisibility(View.GONE)
            view2.setVisibility(View.GONE)
            view3.setVisibility(View.VISIBLE)
         })
        pagerViewAdapter = DetailsActivityAdapter(supportFragmentManager)
        viewPager?.setAdapter(pagerViewAdapter)

        viewPager?.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                onChangeTab(position)
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
    }

    private fun onChangeTab(position: Int) {
        if (position == 0) {
            var view1: View = findViewById(R.id.view1)
            var view2: View = findViewById(R.id.view2)
            var view3: View = findViewById(R.id.view3)
            view1.setVisibility(View.VISIBLE)
            view2.setVisibility(View.GONE)
            view3.setVisibility(View.GONE)
            chat!!.textSize = 20f
            status!!.textSize = 15f
            call!!.textSize = 15f

        }
        if (position == 1) {
            var view1: View = findViewById(R.id.view1)
            var view2: View = findViewById(R.id.view2)
            var view3: View = findViewById(R.id.view3)
            view1.setVisibility(View.GONE)
            view2.setVisibility(View.VISIBLE)
            view3.setVisibility(View.GONE)
            chat!!.textSize = 15f
            status!!.textSize = 20f
            call!!.textSize = 15f

        }
        if (position == 2) {
            var view1: View = findViewById(R.id.view1)
            var view2: View = findViewById(R.id.view2)
            var view3: View = findViewById(R.id.view3)
            view1.setVisibility(View.GONE)
            view2.setVisibility(View.GONE)
            view3.setVisibility(View.VISIBLE)
            chat!!.textSize = 15f
            status!!.textSize = 15f
            call!!.textSize = 20f
        }
    }
}