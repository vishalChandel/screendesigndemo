package com.shadow.screendesigndemo.Activities


import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.shadow.screendesigndemo.R

class PerformanceActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_performance)

       var pieChartIV=findViewById<ImageView>(R.id.pieChartIV)
        pieChartIV.setOnClickListener {
            var intent=Intent(this,SearchByActivity::class.java)
            this.startActivity(intent)
        }
        var backIV3= findViewById<ImageView>(R.id.backIV3)!!
        backIV3.setOnClickListener{
            val intent = Intent(this, ProfileActivity::class.java)
            this.startActivity(intent)
        }
        val languages = resources.getStringArray(R.array.Languages)

        // access the spinner
        val spinner = findViewById<Spinner>(R.id.spinner)
        if (spinner != null) {
            val adapter = ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item, languages
            )
            spinner.adapter = adapter

            spinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View, position: Int, id: Long
                ) {
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }
        }
    }
}

