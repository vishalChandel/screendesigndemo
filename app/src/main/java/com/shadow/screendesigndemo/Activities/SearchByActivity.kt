package com.shadow.screendesigndemo.Activities

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shadow.screendesigndemo.Adapter.SearchByListAdapter
import com.shadow.screendesigndemo.R
import kotlinx.android.synthetic.main.searchby_list.*

class SearchByActivity : AppCompatActivity() {
    var searchByListAdapter: SearchByListAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_by)

        var backIV2= findViewById<ImageView>(R.id.backIV2)!!
        backIV2.setOnClickListener{
            val intent = Intent(this, NavigationDrawerActivity::class.java)
            this.startActivity(intent)
        }
        var recyclerView: RecyclerView = findViewById(R.id.searchByRecyclerView)

        // setting recyclerView layoutManager
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL, false
        )
        recyclerView.setLayoutManager(layoutManager)
        searchByListAdapter = SearchByListAdapter()

        // setting recycle view adapter
        recyclerView.setAdapter(searchByListAdapter)
    }
}