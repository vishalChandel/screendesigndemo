package com.shadow.screendesigndemo.Activities

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shadow.screendesigndemo.Adapter.AccountListAdapter
import com.shadow.screendesigndemo.R


class AccountsActivity : AppCompatActivity() {
    var accountListAdapter: AccountListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_accounts)
        var recyclerView: RecyclerView = findViewById(R.id.accountRecyclerView)


        // setting recyclerView layoutManager
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.HORIZONTAL, false
        )

        recyclerView.setLayoutManager(layoutManager)
        accountListAdapter = AccountListAdapter()
        recyclerView.setAdapter(accountListAdapter)
        var accountProfileIV:ImageView=findViewById(R.id.accountProfileIV)
        accountProfileIV.setOnClickListener {
            showCustomAlert()
        }
    }

    fun showCustomAlert() {
        val alertDialog = Dialog(this)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.custom_alert_box)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        // set the custom dialog components - text, image and button

        alertDialog.show()

    }
}

