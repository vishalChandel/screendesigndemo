package com.shadow.screendesigndemo.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.shadow.screendesigndemo.R

class AccountListAdapter : RecyclerView.Adapter<AccountListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.account_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 2
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var accountListTV1: TextView
        var accountListTV2: TextView
        var accountListTV3: TextView
        var accountListTV4: TextView
        var accountListTV5: TextView
        var accountListTV6: TextView
        var accountListTV7: TextView
        var accountListTV8: TextView
        var accountListTV9: TextView
        var accountListTV10: TextView
        var accountListTV11: TextView
        var accountListIV1: ImageView
        var accountListIV2: ImageView
        var accountListIV3: ImageView
        var accountListIV4: ImageView


        init {
            accountListIV1 = itemView.findViewById(R.id.accountListIV1)
            accountListIV2 = itemView.findViewById(R.id.accountListIV2)
            accountListIV3 = itemView.findViewById(R.id.accountListIV3)
            accountListIV4 = itemView.findViewById(R.id.accountListIV4)
            accountListTV1 = itemView.findViewById(R.id.accountListTV1)
            accountListTV2 = itemView.findViewById(R.id.accountListTV2)
            accountListTV3 = itemView.findViewById(R.id.accountListTV3)
            accountListTV4 = itemView.findViewById(R.id.accountListTV4)
            accountListTV5 = itemView.findViewById(R.id.accountListTV5)
            accountListTV6 = itemView.findViewById(R.id.accountListTV6)
            accountListTV7 = itemView.findViewById(R.id.accountListTV7)
            accountListTV8 = itemView.findViewById(R.id.accountListTV8)
            accountListTV9 = itemView.findViewById(R.id.accountListTV9)
            accountListTV10 = itemView.findViewById(R.id.accountListTV10)
            accountListTV11 = itemView.findViewById(R.id.accountListTV11)
        }
    }
}