package com.shadow.screendesigndemo.Adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.shadow.screendesigndemo.R
import java.util.*

class TaskList1Adapter : RecyclerView.Adapter<TaskList1Adapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.task_list_1, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val rnd = Random()
        val currentColor: Int = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
        holder.parent.setBackgroundColor(currentColor)
    }
    override fun getItemCount(): Int {
        return 5
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var taskListIV1: ImageView
        var taskListTV1: TextView
        var taskListTV2: TextView
        var parent: LinearLayout


        init {

            taskListIV1 = itemView.findViewById(R.id.taskListIV1)
            taskListTV1 = itemView.findViewById(R.id.taskListTV1)
            taskListTV2 = itemView.findViewById(R.id.taskListTV2)
            parent=itemView.findViewById(R.id.parentLLTL1)

        }
    }

}