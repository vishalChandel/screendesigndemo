package com.shadow.screendesigndemo.Adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.shadow.screendesigndemo.R
import java.util.*

class TaskList2Adapter : RecyclerView.Adapter<TaskList2Adapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.task_list_2, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) { val rnd = Random()
        val currentColor: Int = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
        holder.parent.setBackgroundColor(currentColor)}
    override fun getItemCount(): Int {
        return 5
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var taskList2IV1: ImageView
        var taskList2TV1: TextView
        var taskList2TV2: TextView
        var parent: LinearLayout


        init {

            taskList2IV1 = itemView.findViewById(R.id.taskList2IV1)
            taskList2TV1 = itemView.findViewById(R.id.taskList2TV1)
            taskList2TV2 = itemView.findViewById(R.id.taskList2TV2)
            parent=itemView.findViewById(R.id.parentLLTL2)

        }
    }



}