package com.shadow.screendesigndemo.Adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.shadow.screendesigndemo.R
import java.util.*

class DesignerListAdapter : RecyclerView.Adapter<DesignerListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.designer_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val rnd = Random()
        val currentColor: Int = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
        holder.parent.setBackgroundColor(currentColor)
    }
    override fun getItemCount(): Int {
        return 20
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var image: ImageView
        var parent:RelativeLayout
        var title: TextView
        var titleText: TextView
        var name: TextView
        var ranking: TextView
        var rankingText: TextView
        var popularity: TextView
        var popularityText: TextView
        var like: TextView
        var likeText: TextView
        var follower: TextView
        var folloerText: TextView
        var dot: ImageView


        init {
            image = itemView.findViewById(R.id.imageIV)
            title = itemView.findViewById(R.id.titleTV)
            titleText = itemView.findViewById(R.id.titleTextTV)
            name = itemView.findViewById(R.id.nameTV)
            dot = itemView.findViewById(R.id.dotTV)
            ranking = itemView.findViewById(R.id.rankingTV)
            rankingText = itemView.findViewById(R.id.rankingTextTV)
            popularity = itemView.findViewById(R.id.popularityTV)
            popularityText = itemView.findViewById(R.id.popularityTextTV)
            like = itemView.findViewById(R.id.likeTV)
            likeText = itemView.findViewById(R.id.likeTextTV)
            folloerText = itemView . findViewById(R.id.followerTextTV)
            follower=itemView.findViewById(R.id.followerTV)
            parent=itemView.findViewById(R.id.parentRL)

        }
    }
}