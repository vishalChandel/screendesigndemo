package com.shadow.screendesigndemo.Adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.shadow.screendesigndemo.Fragments.AttentionFragment
import com.shadow.screendesigndemo.Fragments.CategoryFragment
import com.shadow.screendesigndemo.Fragments.DesignerFragment

class DetailsActivityAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        var fragment: Fragment? = null
        when (position) {
            0 -> fragment = DesignerFragment()
            1 -> fragment = CategoryFragment()
            2 -> fragment = AttentionFragment()
        }
        return fragment!!
    }

    override fun getCount(): Int {
        return 3
    }
}
