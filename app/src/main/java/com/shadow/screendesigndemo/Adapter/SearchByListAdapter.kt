package com.shadow.screendesigndemo.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.shadow.screendesigndemo.R

class SearchByListAdapter : RecyclerView.Adapter<SearchByListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.searchby_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {}
    override fun getItemCount(): Int {
        return 5
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var searchByIV: ImageView
        var searchByTV:TextView
        var searchByTV1:TextView
        var searchByForwardIV:ImageView

        init {

            searchByIV = itemView.findViewById(R.id.searchByIV)
            searchByTV = itemView.findViewById(R.id.searchBYTV)
            searchByTV1 = itemView.findViewById(R.id.searchBYTV1)
            searchByForwardIV=itemView.findViewById(R.id.searchByForwardIV)
        }
    }


}