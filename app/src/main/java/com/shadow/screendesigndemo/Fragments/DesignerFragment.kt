package com.shadow.screendesigndemo.Fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shadow.screendesigndemo.Adapter.DesignerListAdapter
import com.shadow.screendesigndemo.R


class DesignerFragment : Fragment() {
    var recyclerView: RecyclerView? = null
    var designerListAdapter: DesignerListAdapter? = null
  lateinit var ctx:Context
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_designer, container, false)
        initRecyclerView1(view)
        return view

    }
    private fun initRecyclerView1(view: View?) {
        recyclerView = view?.findViewById(R.id.designerReyclerview)
        recyclerView?.setLayoutManager(LinearLayoutManager(activity))
        designerListAdapter = DesignerListAdapter()
        recyclerView?.setAdapter(designerListAdapter)
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
        recyclerView?.layoutManager=layoutManager
        recyclerView!!.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))


    }

}